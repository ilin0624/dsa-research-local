import pandas as pd
def parse_taxi_data(filepath):
    '''
    Parses .txt file of taxi coordinates, where each line is in the format:
    taxi id, date time, longitude, latitude.

    Parameters
    ----------
    filepath : str
        file path of the .txt file being parsed

    Returns
    -------
    list, list
        list of coordinate pairs, the trajectory's bounds
    '''


    data = pd.read_csv(filepath, names=['id', 'date and time', 'longitude', 'latitude'])

    return data

def find_midpoint(df):
    midpoints = []
    for i in range(len(df[:-2])):
        x1 = df.loc[i, 'longitude']
        y1= df.loc[i, 'latitude']
        x2 = df.loc[i+1, 'longitude']
        y2 = df.loc[i+1, 'latitude']
        long = (x1+x2) / 2
        lat = (y1+y2) / 2
        point = [long, lat]
        midpoints.append(point)
    return midpoints
def create_midpoints_df(midpoints_list):
    midpoints_df = pd.DataFrame(midpoints_list, columns =['Longitude', 'Latitude'])
    return midpoints_df
def create_csv_file(midpoints_df, df):
    file_name = str(df.loc[0]['id'])
    return midpoints_df.to_csv(file_name)



def main():
    taxi_trajectory = 'taxi1.txt'
    trajectory_df = parse_taxi_data(taxi_trajectory)
    midpoints = find_midpoint(trajectory_df)
    midpoints_df = create_midpoints_df(midpoints)
    create_csv_file(midpoints_df, trajectory_df)




if __name__ == "__main__":
    main()
