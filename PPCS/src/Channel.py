class Channel:

    def __init__(self, channel_id):
        self._channel_id = channel_id
        
        @property
        def availability(self):
            return _availability

        @availability.setter
        def availability(self, availability):
            self._availability = availability

        @property
        def channel_id(self):
            return _channel_id

        @channel_id.setter
        def channel_id(self, channel_id):
            self._channel_id = channel_id

        @property
        def cell_id(self):
            return _cell_id

        @cell_id.setter
        def cell_id(self, cell_id):
            self._cell_id = cell_id
