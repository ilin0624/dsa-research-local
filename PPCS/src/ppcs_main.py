from sympy.geometry import Segment, Circle, intersection
from sympy.functions.elementary.integers import ceiling
from collections import Counter
import Report as rpt
import Query as qry
import Cell as cl
import Worker as wk
import math
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import itertools
import Channel as ch
import selectors
import random

def parse_geolife_data(filepath):
    '''
    Parses .txt file of a user's coordinates, where each line is in the format:
    latitude,longitude,0,altitude,days,date,time

    Parameters
    ----------
    filepath : str
        file path of the .txt file being parsed
    
    Returns
    -------
    list, list
        list of coordinate pairs, the trajectory's bounds
    '''
    longitude = []
    latitude = []
    coordinates = []
    boundaries = []

    data = pd.read_csv(filepath, names=['latitude', 'longitude', '0', 'altitude', 'days', 'date', 'time'])

    for x in data['longitude']:
        longitude.append(x)
    for y in data['latitude']:
        latitude.append(y)

    x_min = min(longitude)
    x_max = max(longitude)
    y_min = min(latitude)
    y_max = max(latitude)
    
    boundaries.append(x_min)
    boundaries.append(x_max)
    boundaries.append(y_min)
    boundaries.append(y_max)
    # boundaries = [x_min, x_max, y_min, y_max]

    for z in range(0, len(longitude)):
        coordinates.append([longitude[z], latitude[z]])

    return coordinates, boundaries
    
def generate_cell_id(num):
    '''
    Generates a cell identifier to assign to a new cell
    
    Parameters
    ----------
    num: int
        cell number
    
    Returns
    -------
    String
        cell identifier
    '''
    create_cell_id = 'c-' + str(num)
    return create_cell_id
    
def initialize_worldview(x1, x2, y1, y2, channels):
    '''
    Creates cells for the initial worldview.
    
    Parameters
    ----------
    x1 : double
        minimum longitude of worldview
    x2 : double
        maximum longitude of worldview
    y1 : double
        minimum latitude of worldview
    y2 : double
        maximum latitude of worldview
    channels : int
        the desired number of channels to be assigned to each cell
        
    Returns
    -------
    dictionary
        dictionary where key is cell id and value is the corresponding cell
    '''
    
    worldview = {}
    count_x = (x2-x1)/.00117
    count_x = math.floor(count_x)
    count_y = (y2-y1)/.0009
    count_y = math.floor(count_y)
    
    for x in range(1, count_x+2):
        x_begin = x1 + ((x-1)*.00117)
        x_end = x1 + (x*.00117)
        for y in range (1, count_y+2):
            y_begin = y1 + ((y-1)*.0009)
            y_end = y1 + (y*.0009)
            
            new_id = generate_cell_id(len(worldview)+1)
            new_channels = []
            
            for z in range(0, channels):
                value = random.randint(0,1)
                new_channels.append(value)
            
            new_cell = cl.Cell(new_id, x_begin, x_end, y_begin, y_end, new_channels)
            worldview[new_id] = new_cell
    
    return worldview
           
def find_current_cell(worldview, coordinates):
    '''
    Finds the worldview cell that a set of coordinates falls within.
    
    Parameters
    ----------
    worldview: dictionary
        collection of cells to check
    coordinates: list
        longitude/latitude location coordinate pair
    
    Returns
    -------
    Cell
        cell that the given coordinates fall within
    '''
    for x in worldview.values():
        if x.is_in_cell(coordinates):
            return x
    return None

def generate_id(num):
    '''
    Generates a cell identifier to assign to a new user (volunteers and the worker/querying user)
    
    Parameters
    ----------
    num: int
        user number
    
    Returns
    -------
    String
        user identifier
    '''
    create_id = 'u' + str(num)
    return create_id

def all_visited_cells(worldview, coordinates):
    """

    Parameters
    ----------
    worldview
    coordinates

    Returns
    -------
    All cells that visitor visited

    """
    visited_cells = {}

    for i,j in enumerate(coordinates):
        for x in worldview.values():
            if x.is_in_cell(coordinates[i]):
                if x.cell_id not in visited_cells:
                    visited_cells[x.cell_id] = x.channel_info
                    x.set_users_num(x.users_num+1)
        
    return visited_cells

def generate_mbr(worldview, center, radius, trajectory, current_position):
    '''
    Captures cells in a circular shape around a certain point.
        
    Parameters
    ----------
    worldview: list
        all the cells in the world
    center: list
        coordinates that the mbr will be centered on
    radius: double
        size of the mbr
    trajectory: list
        the coordinates of the user's traveled trajectory
    current_position: int
        represents which coordinate the user is currently at in their trajectory

    Returns
    -------
    list, list, int
        identifiers of cells in mbr, the center for the next mbr ([0,0] if at the end of the trajectory), where the user currently is

    '''
    mbr_cells = []
    casted_points = cast_points(center, radius)
    for i,j in enumerate(casted_points):
        for x in worldview.values():
            if x.is_in_cell(casted_points[i]):
                if x.cell_id not in mbr_cells:
                    mbr_cells.append(x.cell_id)
    
    current_point = trajectory[current_position]
    
    while (is_in_circle(center, radius, current_point)) and (current_position < len(trajectory)):
        current_position = current_position + 1
        if current_position < len(trajectory):
            current_point = trajectory[current_position]
    
    if current_position >= len(trajectory):
        return mbr_cells, [0,0], current_position
    else:
        previous_point = trajectory[current_position-1]
        current_point = trajectory[current_position]
        new_center = find_intersection(center, radius, previous_point, current_point)
        return mbr_cells, new_center, current_position

def is_in_circle(center, radius, current_point):
    '''
    Tests to see if a point on the user's trajectory falls within the current mbr
    
    Parameters
    ----------
    center: list
        coordinates that the mbr is centered on
    radius: double
        size of the mbr
    current_point: list
        coordinates from trajectory to be tested

    Returns
    -------
    boolean
        True if the point falls within the mbr and False otherwise
        
    '''
    x_distance = center[0]-current_point[0]
    y_distance = center[1]-current_point[1]
    distance = math.hypot(x_distance, y_distance)
    if distance <= radius:
        return True
    else:
        return False

def cast_points(center, radius):
    '''
    Produces points to identify cells within the boundaries of the mbr
    
    Parameters
    ----------
    center: list
        coordinates that the mbr is centered on
    radius: double
        size of the mbr

    Returns
    -------
    list
        coordinates that will capture the cells within the mbr

    '''
    points = []
    points.append([center[0]+radius, center[1]])
    points.append([center[0]-radius, center[1]])
    
    x_lim = int(ceiling(radius/.00117))
    mid = center
    distance_to_edge = radius
    
    for x in range(0, x_lim): 
        y = 0
        mid = [center[0]+(x*.00117), center[1]]
        if x != 0:
            mid2 = [center[0]-(x*.00117), center[1]]
            distance_to_edge = find_distance_to_edge(x*.00117, radius)
        while (y*.0009) < distance_to_edge:
            points.append([mid[0], center[1]+(y*.0009)])
            if y != 0:
                points.append([mid[0], center[1]-(y*.0009)])
            if x != 0:
                points.append([mid2[0], center[1]+(y*.0009)])
                if y != 0:
                    points.append([mid2[0], center[1]-(y*.0009)])
            y = y + 1
            
        points.append([mid[0], center[1]+distance_to_edge])
        points.append([mid[0], center[1]-distance_to_edge])
        if x != 0:
            points.append([mid2[0], center[1]+distance_to_edge])
            points.append([mid2[0], center[1]-distance_to_edge])
    
    return points
    
def find_distance_to_edge(distance_from_center, radius): 
    '''
    Determines how far a point is from the edge of the mbr

    Parameters
    ----------
    distance_from_center: double
        how far the point is from the center of the mbr
    radius: double
        size of the mbr

    Returns
    -------
    double
        the point's shortest vertical distance from the edge of the mbr

    '''
    b_squared = distance_from_center*distance_from_center
    c_squared = radius*radius
    c_squared_minus_b_squared = c_squared-b_squared
    a = math.sqrt(c_squared_minus_b_squared)
    return a

def find_intersection(center, radius, start, end):
    '''
    Finds the point where a user's traveled trajectory intersects the mbr edge
    
    Parameters
    ----------
    center: list
        coordinates that the mbr is centered on
    radius: double
        size of the mbr
    start: list
        user's most recent coordinates that are still in the mbr
    end: list
        coordinates outside the mbr that the user intends to travel to

    Returns
    -------
    list
        coordinates of the point of intersection

    '''
    circle = Circle((center[0], center[1]), radius)
    line = Segment((start[0], start[1]), (end[0], end[1]))
    intersecting_point = intersection(circle, line)
    
    if len(intersecting_point) > 0:
        inter_x = round(intersecting_point[0].x, 5)
        inter_y = round(intersecting_point[0].y, 5)
        return [inter_x, inter_y]
    else:
        return None
        
def stop_n_sense_count(query, volunteers, worldview):
    volunteers_cells = []
    stop_n_sense = 0
    for v in volunteers:
        visited = all_visited_cells(worldview, v.trajectory)
        v.set_visited_cells(visited)
        visited_cells = v.get_visited_cells()
        for cell in visited_cells:
            volunteers_cells.append(cell)
    
    volunteer_cells_set = set(volunteers_cells)

    for cell in query.mbr_cells:
        if cell not in volunteer_cells_set:
            stop_n_sense += 1
    return stop_n_sense
    

def get_query_cell_count(worker):
    current_position = 0
    mbr_cells = worker.get_visited_cells()
    query = qry.Query(mbr_cells)

    for cell in query.mbr_cells:
        current_position += 1

    return current_position

def majority_knowledge(reported_values):
    summary_report = []
    for x in range(0, len(reported_values)):
        frequency = {"0":0, "1":0, "None":0} # will count the frequency of [zeroes, ones, None/no reported availability]
        current_channel = reported_values[x]
        for y in range(0, len(current_channel)):
            if current_channel[y] == 0:
                frequency["0"] = frequency["0"] + 1
            
            if current_channel[y] == 1:
                frequency["1"] = frequency["1"] + 1
            
            if current_channel[y] is None:
                frequency["None"] = frequency["None"] + 1
        consensus = max(frequency, key = lambda x: frequency[x])
        if consensus is "None":
            summary_report.append(None)
        else:
            summary_report.append(int(consensus))
    return summary_report   

def single_user_knowledge(reported_values):
    summary_report = []
    for x in range(0, len(reported_values)):
        frequency = {"0":0, "1":0, "None":0} # will count the frequency of [zeroes, ones, None/no reported availability]
        current_channel = reported_values[x]
        for y in range(0, len(current_channel)):
            if current_channel[y] == 0:
                frequency["0"] = frequency["0"] + 1
            
            if current_channel[y] == 1:
                frequency["1"] = frequency["1"] + 1
            
            if current_channel[y] is None:
                frequency["None"] = frequency["None"] + 1
        
        if (frequency["0"] == 0 and frequency["1"] == 0) or (frequency["0"] == frequency["1"]):
            consensus = "None"
        else:
            if frequency["0"] > frequency["1"]:
                consensus = "0"
            else:
                consensus = "1"
                
        if consensus is "None":
            summary_report.append(None)
        else:
            summary_report.append(int(consensus))
    return summary_report        

def is_fraudulent(probability): # method with a set probability for fradulent reporting 
    value = random.randint(0, 99)
    if value < probability: 
        return True
    else:
        return False 

def produce_reports(cells, volunteers, size, user, num_of_channels, report_type, fraud_prob):
    # returns a list of cell_ids with no meaningful data
    # if there is meaningful data, it is appended to the user's local cache

    no_meaningful_data = []
    meaningful_data = []
    
    for c in cells:
        cell_report = []
        for i in range(0, len(volunteers), size):
            cluster = volunteers[i:i + size]
            cluster_report = []
            for j in range(0, len(cluster)):
                current_volunteer = cluster[j]
                if len(cluster_report) is 0:
                    if c in current_volunteer.local_cache:
                        local_cache_info = current_volunteer.local_cache[c]
                        for k in range(0, len(local_cache_info)):
                            misreport = is_fraudulent(fraud_prob)
                            if misreport is True and local_cache_info[k] is not None:
                                if local_cache_info[k] == 0:
                                    cluster_report.append([1])
                                else:
                                    cluster_report.append([0])
                            else:    
                                cluster_report.append([local_cache_info[k]])
                    else:
                        for l in range(0, num_of_channels):
                            cluster_report.append([None])
                else:
                    if c in current_volunteer.local_cache:
                        local_cache_info = current_volunteer.local_cache[c]
                        for m in range(0, len(local_cache_info)):
                            misreport = is_fraudulent(fraud_prob)
                            if misreport is True and local_cache_info[m] is not None:
                                if local_cache_info[m] == 0:
                                    cluster_report[m].append(1)
                                else:
                                    cluster_report[m].append(0)
                            else:
                                cluster_report[m].append(local_cache_info[m])
                    else:
                        for n in range(0, num_of_channels):
                            cluster_report[n].append(None)
            
            if report_type is "majority":              
                cluster_majority = majority_knowledge(cluster_report)
            
                if len(cell_report) is 0:
                    for a in range(0, len(cluster_majority)):
                        cell_report.append([cluster_majority[a]])
                else:
                    for b in range(0, len(cluster_majority)):
                        cell_report[b].append(cluster_majority[b])
                        
            if report_type is "single":
                cluster_majority = single_user_knowledge(cluster_report)
            
                if len(cell_report) is 0:
                    for a in range(0, len(cluster_majority)):
                        cell_report.append([cluster_majority[a]])
                else:
                    for b in range(0, len(cluster_majority)):
                        cell_report[b].append(cluster_majority[b])
        
        if report_type is "majority":    
            cell_majority = majority_knowledge(cell_report) # edit accordingly
            if 0 in cell_majority or 1 in cell_majority:
                user.local_cache[c] = cell_majority
                meaningful_data.append(c)
            else:
                no_meaningful_data.append(c)
        if report_type is "single":
            cell_majority = single_user_knowledge(cell_report) # edit accordingly
            if 0 in cell_majority or 1 in cell_majority:
                user.local_cache[c] = cell_majority
                meaningful_data.append(c)
            else:
                no_meaningful_data.append(c)
                
    return no_meaningful_data, meaningful_data

def main():
    
    # program set-up
    
    input_num_of_channels = int(input("Enter how many channels you would like to assign to each cell:"))
    
    x_min = 1000
    x_max = -1000
    y_min = 1000
    y_max = -1000
    
    # create 50 users
    volunteers = []
    for x in range(1, 51):
        filepath = 'C:\\Users\\Ice Lin\\git\\dsa-research-local\\PPCS\\Trajectories\\' + str(x) + '.plt' # may need to adjust the filepaths
        # '/Users/nhidiep/dsa-research/PPCS/Trajectories/'
        num = len(volunteers)+1
        id = 'u-' + str(num)
        coordinates, bounds = parse_geolife_data(filepath)
        if bounds[0] < x_min:
            x_min = bounds[0]
        if bounds[1] > x_max:
            x_max = bounds[1]
        if bounds[2] < y_min:
            y_min = bounds[2]
        if bounds[3] > y_max:
            y_max = bounds[3]
            
        new_user = wk.Worker(id, coordinates)
        volunteers.append(new_user)
    
    # create 51st (querying) user
    worker_coordinates, worker_bounds = parse_geolife_data('C:\\Users\\Ice Lin\\git\\dsa-research-local\\PPCS\\Trajectories\\51.plt')
    # '/Users/nhidiep/dsa-research/PPCS/Trajectories/51.plt'
    if worker_bounds[0] < x_min:
        x_min = worker_bounds[0]
    if worker_bounds[1] > x_max:
       x_max = worker_bounds[1]
    if worker_bounds[2] < y_min:
        y_min = worker_bounds[2]
    if worker_bounds[3] > y_max:
        y_max = worker_bounds[3]
          
    worker = wk.Worker('u-51', worker_coordinates)
    
    # create worldview and populate it with cells
    worldview = initialize_worldview(x_min, x_max, y_min, y_max, input_num_of_channels)
    
    # use worldview to fill local cache of the volunteers and the worker
    
    worker_visited = all_visited_cells(worldview, worker.trajectory)
    trajectory_cells = list(worker_visited.keys())
    worker.set_trajectory_cells(trajectory_cells)
    worker.set_local_cache(worker_visited)  
    
    for y in range(0, len(volunteers)):
        vol = volunteers[y]
        volunteer_visited = all_visited_cells(worldview, vol.trajectory)
        volunteer_trajectory_cells = list(volunteer_visited.keys())
        vol.set_trajectory_cells(volunteer_trajectory_cells)
        vol.set_local_cache(volunteer_visited)
    '''
    # use mbr lookup to expand local cache of each volunteer
    
    for z in range(0, len(volunteers)):
        
        location = 0
        
        other_vol = volunteers[:]
        other_vol.pop(z)
        other_vol.append(worker)
        
        current = volunteers[z]
        next = current.trajectory[0]
        
        print(current.worker_id)
        print(str(len(current.trajectory)))
        current.query_prep(current.trajectory_cells)
            
        while location < len(current.trajectory):
            
            previous_location = location

            captured_cells, next, location = generate_mbr(worldview, next, 0.00234, current.trajectory, location)
            print(str(location))
            if previous_location == location or next is None:
                break
        
            no_meaning, has_meaning = produce_reports(captured_cells, other_vol, 5, current, input_num_of_channels, "majority")
            if len(has_meaning) > 0:
                print("Meaningful info found for " + str(len(has_meaning)) + " cell(s)!")
                
            for n in no_meaning:
                if n in current.sensed and current.sensed[n] is False:
                    current.sensed[n] = True
                    current.local_cache[n] = worldview[n].channel_info
    '''
    # stop and sense/query code

    current_position = 0
    crowd_sensing = 0 # the total number of times that crowd sensing was invoked traversing the test trajectory
    crowd_sensing_2 = 0 # the number of times that the 2nd flavor of crowd sensing  is used
    query_count = 0
    cs_info_for = []
    
    # mbr_cells, next_mbr_center, worker_location = generate_mbr(worldview, geo_coordinates[worker_location], .00234, geo_coordinates, worker_location)
    
    test = input('Enter the radius size to test (Enter a negative number or 0 to quit):')
    radius_to_test = float(test)
    
    while radius_to_test > 0:
        input_num_of_users = int(input("Enter how many users there will be in the system for queries:"))
        experiment_volunteers = volunteers[:input_num_of_users]
        input_cluster_size = int(input("Enter how many users you would like in each cluster (a cluster may be short):"))
        fraud = int(input("Enter the chance of fraud (type as a percent, such as 10 for 10%):"))
        print("Radius size being tested: ", radius_to_test)
        print("Number of users for queries: ", len(experiment_volunteers))
        print("Probability of fraudulent reporting: " + str(fraud) + "%")
        worker.query_prep(worker.trajectory_cells)
        print("Regular stop-and-sense: ", len(worker.sensed))
        next_mbr_center = worker.trajectory[0]
        while current_position < len(worker.trajectory):
            query_count = query_count + 1
            print("Number of crowd-sensing queries sent so far: ", query_count)
            print("Number of stop and senses needed so far: ", crowd_sensing)
            mbr_cells, next_mbr_center, current_position = generate_mbr(worldview, next_mbr_center, radius_to_test, worker.trajectory, current_position)
            print("Number of cells captured in mbr: ", len(mbr_cells))
            print("Captured cells: ", mbr_cells)
        
            meaningless, meaningful = produce_reports(mbr_cells, experiment_volunteers, input_cluster_size, worker, input_num_of_channels, "single", fraud)
            for i in range(0, len(meaningful)):
                if meaningful[i] not in cs_info_for:
                    cs_info_for.append(meaningful[i])
                    
            print("Meaningless cells: ", meaningless)
            print("Querying user's updated cache: ", worker.local_cache)
            print()
            for m in meaningless:
                if m in worker.sensed and worker.sensed[m] is False:
                    worker.sensed[m] = True
                    crowd_sensing = crowd_sensing + 1
                    worker.local_cache[m] = worldview[m].channel_info # will need to edit this when checking report results
            for f in meaningful:
                if f in worker.sensed and worker.sensed[f] is False:
                    if worker.local_cache[f] != worldview[f].channel_info:
                        worker.sensed[f] = True
                        crowd_sensing = crowd_sensing + 1
                        crowd_sensing_2 = crowd_sensing_2 + 1
                    
    
        print("Number of times stop and sense is invoked in response to a meaningless query: ", crowd_sensing)  
        print("Number of stop and senses due to inaccurate reporting: ", crowd_sensing_2) 
        print("Number of meaningful reports: ", len(cs_info_for))   
        print("***")
        new_test = input('Enter another radius size to test:')
        radius_to_test = float(new_test)
        query_count = 0
        crowd_sensing = 0
        crowd_sensing_2 = 0
        inaccurate = 0
        current_position = 0
        cs_info_for = []

    print('COMMANDS: ')
    print('c- display information for a given cell.') # master key for channel info
    print('l- display the size and information in the local cache of a given user') # visited_cells is the local cache
    print('w- display the size/number of cells in the worldview.') # cells numbered from 1 to this number
    print('u- display the number of users in the system (this includes the querying user).') # users numbered from 1 to this number
    print("n- display the number of cells in querier's trajectory")
    print('a- display the number of times querier invoked Stop-N-Sense')
    print('Stop- terminate the system.')
    print()
    command = input('Enter a command:')
    while command != 'Stop':
        if command == 'c':
            c = input('Enter the cell id:')
            if c in worldview:
                cell = worldview[c]
                print('Cell ID: ', cell.cell_id)
                print('Cell Boundaries: ' + str(cell.x_min) + ", " + str(cell.x_max) + ", " + str(cell.y_min) + ", " + str(cell.y_max))
                print('Channel Info: ', cell.channel_info)
                print('Number of Users that have passed through: ', cell.users_num)
            else:
                print('A cell with that id does not exist.')  
            
        if command == 'l':
            u = input('Enter the user id:')
            found = False
            for x in volunteers:
                if x.worker_id == u:
                    print('Local Cache: ', x.local_cache)
                    print('Number of visited cells: ', len(x.trajectory_cells))
                    found = True
                if worker.worker_id == u:
                    print('Local Cache: ', worker.local_cache)
                    print('Number of visited cells: ', len(worker.trajectory_cells))
                    found = True
                    break
            if found == False:
                print('A user with that id does not exist.')
            
        if command == 'w':
            print('Number of cells in the worldview: ', len(worldview))
            
        if command == 'u':
            print('Number of users in the system: ', len(volunteers)+1)

        if command == 'n':
            print("Number of cells along querier's trajectory:", get_query_cell_count(worker))

        if command == 'a':
            print("Number of times querier invoked Stop-N-Sense:", stop_n_sense_count(query, volunteers, worldview))
        # Will probably add commands to help run experiments/tests
        print()
        command = input('Enter another command:')
    print('Program terminated.') 

if __name__ == "__main__":
    main() 
